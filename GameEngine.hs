import System.IO -- for hPutStr and stderr
import Person
import System.Random

-- this holds the changes to the stock value for each turn
nasdaqTrend = [7, 6, -10, 0, -7, 9, 3, 0, 4, 1, -10, -8, 10, 4, 0, 1, 10, 1, -8, -8, 8, 6, -7, 1, -3, 6, -2, -6, 3, -5, 8, 10, -2, -3, -9, -4, 9, -8, -7, -10, -10, -10, 3, -9, 9, -4, -9, 2, -2, -3, -4, 1, -9, 7, -4, -3, 3, -3, -9, 7, -10, -2, -5, -8, 0, -2, -9, 2, 10, -7, -4, 0, 6, 9, -10, -7, 0, 3, 8, -9, -10, -10, 3, 7, -10, 0, 5, -3, 7, -2, 5, -4, -4, 5, -10, 10, 8, 0, 10, -10]
nyseTrend   = [3, 3, -3, 0, -7, 9, 3, 0, 4, 1, -10]
sseTrend    = [-2, 7, -5, 0, -7, 9, 3, 0, 4, 1, -10]
-- use this anasdaq for a shorter game 
-- anasdaq = [1,2,3,4]

-- represents the state of the engine
data State = Normal  { curr :: Person }
           | Invalid { prev :: Person }
           | Terminated

-- prompt the user to enter a single-character command
readCommand :: IO Char
readCommand = do
    hPutStr stderr "       Actions: [B]uy, [S]ell, [W]ait, or [Q]uit: \nSwitch Markets: [N]asdaq,  N[Y]SE, [C]hinese-SSE"
    fmap head getLine

-- prompt the user to enter the initial state
readPerson :: IO State
readPerson = do
    hPutStr stderr "Please enter a name: "
    name <- fmap read getLine
    hPutStr stderr "Please enter starting cash: "
    cash <- fmap read getLine
    return $ Normal (Person name cash 0 100 nasdaqTrend "Nasdaq" nasdaqTrend nyseTrend sseTrend)


-- render the state as text to the console
displayPerson :: State -> IO ()
displayPerson Terminated = return ()
displayPerson (Normal  r) = putStrLn $ "-------\n"++(show r)++"\n"
displayPerson (Invalid r) = putStrLn "\n[!] Please enter one of the specified commands"

-- update the state based on the command character
updatePerson :: State -> Char -> State
updatePerson Terminated _ = Terminated
updatePerson st op
    | null t = Terminated --when user has completed the gmae
    | op == 'b' = if c<v then Normal pers{val=(v+(head cur)),curMarket=(tail cur), nasdaq=(tail t), nyse=(tail ny), sse=(tail s) } else Normal pers{cash=(c-v), qty=(q+1), val=(v+(head cur)), curMarket=(tail cur), nasdaq=(tail t), nyse=(tail ny), sse=(tail s) } --buy
    | op == 's' = if q<1 then Normal pers{val=(v+(head cur)),curMarket=(tail cur), nasdaq=(tail t), nyse=(tail ny), sse=(tail s) } else Normal pers{cash=(c+v), qty=(q-1), val=(v+(head cur)), curMarket=(tail cur), nasdaq=(tail t), nyse=(tail ny), sse=(tail s) } --sell
    | op == 'w' = Normal pers{val=(v+(head cur)), curMarket=(tail cur), nasdaq=(tail t), nyse=(tail ny), sse=(tail s)}
    | op == 'n' = Normal pers{val=(v+(head t)), curMarket=(tail t), curMarketName="Nasdaq", nasdaq=(tail t), nyse=(tail ny), sse=(tail s)}
    | op == 'y' = Normal pers{val=(v+(head ny)), curMarket=(tail ny), curMarketName="NYSE", nasdaq=(tail t), nyse=(tail ny), sse=(tail s)}
    | op == 'c' = Normal pers{val=(v+(head s)), curMarket=(tail s), curMarketName="Chinese-SSE", nasdaq=(tail t), nyse=(tail ny), sse=(tail s)}
    | op == 'q' = Terminated --when user quits
    | otherwise = Invalid pers
    where pers@(Person n c q v cur mname t ny s) = case st of (Normal  r) -> r
                                                              (Invalid r) -> r

-- perform the three steps of the engine loop
theLoop :: State -> IO ()
theLoop Terminated = return ()
theLoop st = do
    displayPerson st
    c <- readCommand
    result <- return $ updatePerson st c
    theLoop result

-- show an introductory message
displayIntro :: IO ()
displayIntro = putStrLn $ "\nWelcom to the trading game!"
					   ++ "\n-------------------------------"
					   ++ "\nThe goal is the generate profit by buying and selling 'stocks'"
					   ++ "\nWe reccomend you start yourself with $500"
					   ++ "\n "
					   ++ "\n each turn you will see the following: "
					   ++ "\n     cash: The amound of money you currently hold"
					   ++ "\n     qty:  The amound of stocks you currently hold"
					   ++ "\n     val:  The current value of the stock on the market"
					   ++ "\n each turn you can choose to buy, sell, or just wait"
					   ++ "\n"
					   ++ "\nHave fun, and try not to go bankrupt"
                       ++ "\n==============================\n"

-- show an exit message
displayOutro :: IO ()
displayOutro = putStrLn $ "\n=============================="
                       ++ "\nThanks for Playing!!!"
                       ++ "\nCopyright (c) 2015 Evan J Hopkins\n"
-- initialize the state and start the loop
main = do
    displayIntro
    initSt <- readPerson
    theLoop initSt
    displayOutro

