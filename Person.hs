module Person where

data Person = Person { name :: [Char], cash :: Int, qty :: Int , val :: Int, curMarket :: [Int], curMarketName :: [Char], nasdaq :: [Int], nyse :: [Int], sse :: [Int]}

instance Show Person where
    show (Person name cash qty val curMarket curMarketName nasdaq nyse sse) = " Cash: $"++(show cash)++" Qty: "++(show qty)++" "++(show curMarketName)++": $"++(show val)

-- cashes out the user when the game is over
finalize :: Person -> Int
finalize (Person name cash qty val curMarket curMarketName nasdaq nyse sse) = cash + (qty*val)
