# Stock Trader #

### Theme & Backstory ###
The goal of stock trader is to generate as much profit as possible given your starting funds. Your job is buy or sell stock in hopes of making a profit. There are different markets,

### World & Locations ###
There are three locations, each of which represent a different stock market. You can hop between them as you choose. Keep in mind they are all running simultaneously, so being one one market means the value of the other two will be changing.

Nasdaq
NYSE
Hopp

### Items & Objects ###
Cash: This is what you use to buy and sell stock. The game will not allow you to buy if you do not have enough cash.
Qty: The amount of stock you hold
Val: the value of the current stock on the current market

### Goals & Obstacles ###
The goal is to make as much money as possible in 100 days